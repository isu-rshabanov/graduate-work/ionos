use std::ops::{Add, AddAssign, Sub, SubAssign};

use float_cmp::approx_eq;

use super::earth_consts;

/// ULP - units of least precision, or units in the last place
const DEFAULT_ULPS: i64 = 6;

#[derive(Debug, Clone, Default)]
pub struct Point {
    pub x: Coordinate,
    pub y: Coordinate,
    pub z: Coordinate,
}

pub type Coordinate = f64;
pub type Distance = f64;
pub type Degree = f64;
pub type Radians = f64;

/// A type representing a point in the XYZ coordinate space
impl Point {
    pub fn new(x: Coordinate, y: Coordinate, z: Coordinate) -> Self {
        Self { x, y, z }
    }

    /// Computes XYZ coordinates from latitude, longitude and altitude
    pub fn from_latlonalt(lat: Degree, lon: Degree, alt: Distance) -> Self {
        let lat = lat.to_radians();
        let lon = lon.to_radians();
        let t = earth_consts::SEMI_MAJOR_AXIS
            / (1.0 - earth_consts::ECCENTRICITY_SQUARE * lat.sin() * lat.sin()).sqrt();

        Self {
            x: (t + alt) * lat.cos() * lon.cos(),
            y: (t + alt) * lat.cos() * lon.sin(),
            z: (t * (1.0 - earth_consts::ECCENTRICITY_SQUARE) + alt) * lat.sin(),
        }
    }

    /// Converts XYZ coordinates to latitude, longitude, altitude
    pub fn to_latlonalt(&self) -> (Degree, Degree, Distance) {
        let r = (self.x.powi(2) + self.y.powi(2)).sqrt();

        let lon = self.y.atan2(self.x);
        let mut lat = (self.z / r).atan();
        let mut alt;

        let mut c;
        for _ in 0..15 {
            c = (1.0 - earth_consts::ECCENTRICITY_SQUARE * lat.sin() * lat.sin())
                .sqrt()
                .recip();
            alt = self.z
                + earth_consts::SEMI_MAJOR_AXIS * c * earth_consts::ECCENTRICITY_SQUARE * lat.sin();
            lat = (alt / r).atan();
        }

        c = (1.0 - earth_consts::ECCENTRICITY_SQUARE * lat.sin() * lat.sin())
            .sqrt()
            .recip();
        alt = r / lat.cos() - earth_consts::SEMI_MAJOR_AXIS * c;

        (lat.to_degrees(), lon.to_degrees(), alt)
    }

    /// Computes XYZ coordinates from altitude (elevation), azimuth and radius-vector
    pub fn with_altazr(&self, mut alt: Degree, mut az: Degree, r: Distance) -> Self {
        alt = alt.to_radians();
        az = az.to_radians();

        let (lat, lon) = self.to_latlon_radians();

        let delt = (alt.sin() * lat.sin() + az.cos() * alt.cos() * lat.cos()).asin();
        let th = -(-az.sin() * alt.cos())
            .atan2(alt.sin() * lat.cos() - az.cos() * alt.cos() * lat.sin());

        Self {
            x: self.x + r * delt.cos() * (lon + th).cos(),
            y: self.y + r * delt.cos() * (lon + th).sin(),
            z: self.z + r * delt.sin(),
        }
    }

    /// Converts XYZ coordinates to altitude (elevation), azimuth and radius-vector
    pub fn to_altazr(&self, other: &Point) -> (Degree, Degree, Distance) {
        let (dx, dy, dz) = (other.x - self.x, other.y - self.y, other.z - self.z);
        let r = (dx.powi(2) + dy.powi(2) + dz.powi(2)).sqrt();

        let (lat, lon) = self.to_latlon_radians();

        let delt = (dz / r).asin();
        let th = lon - dy.atan2(dx);

        let alt = std::f64::consts::FRAC_PI_2
            - (delt.sin() * lat.sin() + lat.cos() * delt.cos() * th.cos()).acos();

        let x = lat.sin() * delt.cos() * th.cos() - lat.cos() * delt.sin();
        let y = delt.cos() * th.sin();

        let mut az = (-y).atan2(-x);
        az = if az.is_sign_negative() {
            az + std::f64::consts::TAU
        } else {
            az
        };

        (alt.to_degrees(), az.to_degrees(), r)
    }

    #[inline]
    fn to_latlon_radians(&self) -> (Radians, Radians) {
        let (lat, lon, _) = self.to_latlonalt();
        (lat.to_radians(), lon.to_radians())
    }

    pub fn at_distance(&self, distance: Distance) -> Self {
        const BEARING: f64 = 0.0;
        let (mut olat, mut olon, oalt) = self.to_latlonalt();
        olat = olat.to_radians();
        olon = olon.to_radians();

        let angular_distance = distance / earth_consts::SEMI_MAJOR_AXIS;
        let lat = (olat.sin() * angular_distance.cos()
            + olat.cos() * angular_distance.sin() * BEARING.cos())
        .asin();
        let lon = olon
            + (BEARING.sin() * angular_distance.sin() * olat.cos())
                .atan2(angular_distance.cos() - olat.sin() * lat.sin());

        let alt = oalt + distance;

        Self::from_latlonalt(lat.to_degrees(), lon.to_degrees(), alt)
    }
}

impl PartialEq for Point {
    fn eq(&self, other: &Self) -> bool {
        approx_eq!(Coordinate, self.x, other.x, ulps = DEFAULT_ULPS)
            && approx_eq!(Coordinate, self.y, other.y, ulps = DEFAULT_ULPS)
            && approx_eq!(Coordinate, self.z, other.z, ulps = DEFAULT_ULPS)
    }
}

impl Eq for Point {}

impl From<(Coordinate, Coordinate, Coordinate)> for Point {
    fn from(value: (Coordinate, Coordinate, Coordinate)) -> Self {
        Self::new(value.0, value.1, value.2)
    }
}

impl Sub for Point {
    type Output = Self;

    fn sub(mut self, rhs: Self) -> Self::Output {
        self -= rhs;
        self
    }
}

impl SubAssign for Point {
    fn sub_assign(&mut self, rhs: Self) {
        self.x -= rhs.x;
        self.y -= rhs.y;
        self.z -= rhs.z;
    }
}

impl Sub<&Self> for Point {
    type Output = Self;

    fn sub(mut self, rhs: &Self) -> Self::Output {
        self -= rhs;
        self
    }
}

impl SubAssign<&Self> for Point {
    fn sub_assign(&mut self, rhs: &Self) {
        self.x -= rhs.x;
        self.y -= rhs.y;
        self.z -= rhs.z;
    }
}

impl Add for Point {
    type Output = Self;

    fn add(mut self, rhs: Self) -> Self::Output {
        self += rhs;
        self
    }
}

impl AddAssign for Point {
    fn add_assign(&mut self, rhs: Self) {
        self.x += rhs.x;
        self.y += rhs.y;
        self.z += rhs.z;
    }
}

impl Add<&Self> for Point {
    type Output = Self;

    fn add(mut self, rhs: &Self) -> Self::Output {
        self += rhs;
        self
    }
}

impl AddAssign<&Self> for Point {
    fn add_assign(&mut self, rhs: &Self) {
        self.x += rhs.x;
        self.y += rhs.y;
        self.z += rhs.z;
    }
}

#[cfg(test)]
mod tests {
    use float_cmp::assert_approx_eq;

    use super::*;

    fn get_latlonalt() -> (Degree, Degree, Distance) {
        (52.88, 103.21, 0.5)
    }

    fn get_point_pair() -> (Point, Point) {
        let p1 = Point::from_latlonalt(52.88, 103.21, 0.5);
        let p2 = Point::from_latlonalt(52.5, 102.0, 100.0);

        (p1, p2)
    }

    #[test]
    fn new() {
        let x = 0.0_f64;
        let y = 1.0_f64;
        let z = 2.0_f64;

        let point = Point::new(x, y, z);
        assert_approx_eq!(Coordinate, x, point.x, ulps = DEFAULT_ULPS);
        assert_approx_eq!(Coordinate, y, point.y, ulps = DEFAULT_ULPS);
        assert_approx_eq!(Coordinate, z, point.z, ulps = DEFAULT_ULPS);
    }

    #[test]
    fn eq() {
        let (lhs, rhs) = (Point::default(), Point::default());

        assert_eq!(lhs, rhs);
    }

    #[test]
    fn from_latlonalt() {
        let (lat, lon, alt) = get_latlonalt();
        let expected = Point::new(-881.5505220796239, 3755.5613640621414, 5062.894259150701);

        let actual = Point::from_latlonalt(lat, lon, alt);

        assert_eq!(expected, actual);
    }

    #[test]
    fn to_latlonalttest() {
        let (expected_lat, expected_lon, expected_alt) = get_latlonalt();
        let point = Point::from_latlonalt(expected_lat, expected_lon, expected_alt);

        let (lat, lon, alt) = point.to_latlonalt();

        assert_approx_eq!(Degree, expected_lat, lat, ulps = DEFAULT_ULPS);
        assert_approx_eq!(Degree, expected_lon, lon, ulps = DEFAULT_ULPS);

        // strangely, approx_eq failes
        let (ex_alt_abs, alt_abs) = (expected_alt.abs(), alt.abs());
        let abs_diff = (ex_alt_abs - alt_abs).abs()
            / [ex_alt_abs, alt_abs]
                .iter()
                .cloned()
                .fold(f64::NAN, f64::min);

        assert!(abs_diff < 10.0_f64.powi(-DEFAULT_ULPS as i32));
    }

    #[test]
    fn with_altazr() {
        let (point, expected) = get_point_pair();
        let (alt, az, r) = point.to_altazr(&expected);

        let actual = point.with_altazr(alt, az, r);

        assert_eq!(expected, actual);
    }

    #[test]
    fn to_altazr() {
        let p1 = Point::from_latlonalt(52.88, 103.21, 0.5);
        let p2 = Point::from_latlonalt(52.5, 102.0, 100.0);
        let (expected_alt, expected_az, expected_r) =
            (46.575439454145986, 243.1492660088922, 136.0711663951855);

        let (alt, az, r) = p1.to_altazr(&p2);

        assert_approx_eq!(Degree, expected_alt, alt, ulps = DEFAULT_ULPS);
        assert_approx_eq!(Degree, expected_az, az, ulps = DEFAULT_ULPS);
        assert_approx_eq!(Degree, expected_r, r, ulps = DEFAULT_ULPS);
    }

    #[test]
    fn from_into() {
        let xyz = (0.0, 0.0, 0.0);
        let expected_point = Point::new(0.0, 0.0, 0.0);

        let actual_point1: Point = xyz.into();
        let actual_point2 = Point::from(xyz);

        assert_eq!(expected_point, actual_point1);
        assert_eq!(expected_point, actual_point2);
    }

    #[test]
    fn at_distance() {
        let distance = 50.0; // km
        let (lat, lon, alt) = get_latlonalt();
        let origin = Point::from_latlonalt(lat, lon, alt);
        let dest = origin.at_distance(distance);

        let (_, _, oalt) = origin.to_latlonalt();
        let (_, _, dalt) = dest.to_latlonalt();

        assert_approx_eq!(Degree, (oalt - dalt).abs(), distance, ulps = DEFAULT_ULPS);
    }

    #[test]
    fn add() {
        let lhs = Point::new(0.0, 0.0, 0.0);
        let rhs = Point::new(1.0, 1.0, 1.0);
        let expected = Point::new(1.0, 1.0, 1.0);

        let ref_actual = lhs.clone() + &rhs;
        let actual = lhs + rhs;

        assert_eq!(expected, actual);
        assert_eq!(expected, ref_actual);
    }

    #[test]
    fn sub() {
        let lhs = Point::new(0.0, 0.0, 0.0);
        let rhs = Point::new(1.0, 1.0, 1.0);
        let expected = Point::new(-1.0, -1.0, -1.0);

        let ref_actual = lhs.clone() - &rhs;
        let actual = lhs - rhs;

        assert_eq!(expected, actual);
        assert_eq!(expected, ref_actual);
    }

    #[test]
    fn add_assign() {
        let mut lhs = Point::new(0.0, 0.0, 0.0);
        let rhs = Point::new(1.0, 1.0, 1.0);
        let expected = Point::new(1.0, 1.0, 1.0);
        let expected2 = Point::new(2.0, 2.0, 2.0);

        lhs += &rhs;
        assert_eq!(expected, lhs);

        lhs += rhs;
        assert_eq!(expected2, lhs);
    }

    #[test]
    fn sub_assign() {
        let mut lhs = Point::new(0.0, 0.0, 0.0);
        let rhs = Point::new(1.0, 1.0, 1.0);
        let expected = Point::new(-1.0, -1.0, -1.0);
        let expected2 = Point::new(-2.0, -2.0, -2.0);

        lhs -= &rhs;
        assert_eq!(expected, lhs);

        lhs -= rhs;
        assert_eq!(expected2, lhs);
    }
}
