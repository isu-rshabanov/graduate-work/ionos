use std::{
    fs::File,
    io::{BufRead, BufReader},
    path::Path,
};

use crate::{geom::Point, FromFileByLines};

pub use self::{station::*, station_record::StationRecord};

use chrono::NaiveDateTime;
use thiserror::Error;

pub mod area;
pub mod station;
pub mod station_record;

#[derive(Debug, Error)]
pub enum ParseErrorKind {
    #[error("invalid datetime: {0:?}")]
    InvalidDatetime(#[from] chrono::ParseError),
    #[error("invalid location")]
    InvalidLocation,
}

pub struct SimpleStationReader;

impl FromFileByLines for SimpleStationReader {
    type Err = StationError;

    type Item = StationRecord;

    type Target = Station;

    fn parse_item_str(record: &str) -> Result<Self::Item, Self::Err> {
        const DATETIME_PATTERN: &str = "%Y%m%d\t%H%M%S";

        let iter = record.rsplitn(4, char::is_whitespace);
        let datetime = iter
            .clone()
            .last()
            .map(|x| {
                NaiveDateTime::parse_from_str(x, DATETIME_PATTERN)
                    .map_err(ParseErrorKind::InvalidDatetime)
            })
            .unwrap()?;
        let data = iter
            .take(3)
            .map(|x| {
                x.parse::<f64>()
                    .map_err(|_| StationError::Parse(ParseErrorKind::InvalidLocation))
                    .unwrap()
            })
            .collect::<Vec<_>>();

        let location = Point::from_latlonalt(data[2], data[1], data[0]);

        Ok(StationRecord::new(datetime, location))
    }

    fn from_file_by_lines<P: AsRef<Path>>(path: P) -> Result<Self::Target, Self::Err> {
        let file = File::open(path).map_err(StationError::IO)?;
        let lines = BufReader::new(file).lines().skip(1);

        let mut records = if let (_, Some(capacity)) = lines.size_hint() {
            Vec::with_capacity(capacity)
        } else {
            Vec::default()
        };

        for line in lines {
            let line = line.map_err(StationError::IO)?;
            records.push(Self::parse_item_str(&line)?);
        }

        Ok(records.into_iter().collect())
    }
}

#[cfg(test)]
mod tests {
    use std::path::PathBuf;

    use chrono::{NaiveDate, Timelike};

    use crate::FromFile;

    use super::*;

    impl FromFile for SimpleStationReader {
        type Err = StationError;

        type Item = StationRecord;

        type Target = Station;

        fn parse_item(record: &[u8]) -> Result<Self::Item, Self::Err> {
            let record = std::str::from_utf8(record).unwrap();

            const DATETIME_PATTERN: &str = "%Y%m%d\t%H%M%S";

            let iter = record.rsplitn(4, char::is_whitespace);
            let datetime = iter
                .clone()
                .last()
                .map(|x| {
                    NaiveDateTime::parse_from_str(x, DATETIME_PATTERN)
                        .map_err(ParseErrorKind::InvalidDatetime)
                })
                .unwrap()?;
            let data = iter
                .take(3)
                .map(|x| x.parse::<f64>().unwrap())
                .collect::<Vec<_>>();

            let location = Point::from_latlonalt(data[2], data[1], data[0]);

            Ok(StationRecord::new(datetime, location))
        }

        fn from_file<P: AsRef<Path>>(path: P, delim: u8) -> Result<Self::Target, Self::Err> {
            let file = File::open(path).map_err(StationError::IO)?;
            let splits = BufReader::new(file).split(delim).skip(1);

            let mut records = if let (_, Some(capacity)) = splits.size_hint() {
                Vec::with_capacity(capacity)
            } else {
                Vec::default()
            };

            for record_bytes in splits {
                let record_bytes = record_bytes.map_err(StationError::IO)?;
                records.push(Self::parse_item(&record_bytes)?);
            }

            Ok(records.into_iter().collect())
        }
    }

    fn get_station() -> Station {
        let datetime = NaiveDate::from_ymd_opt(2015, 1, 1)
            .unwrap()
            .and_hms_opt(0, 0, 0)
            .unwrap();

        vec![
            StationRecord::new(
                datetime,
                Point::from_latlonalt(41.186897, -125.424376, 410.365252),
            ),
            StationRecord::new(
                datetime.with_second(1).unwrap(),
                Point::from_latlonalt(41.149965, -125.357935, 410.352054),
            ),
        ]
        .into_iter()
        .collect()
    }

    #[test]
    fn from_file_by_lines_test() {
        let dir = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        let path = dir.join("test/station.dat");
        let expected_station = get_station();

        let station = SimpleStationReader::from_file_by_lines(path).unwrap();

        assert_eq!(expected_station, station);
    }

    #[test]
    fn from_files_test() {
        let dir = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        let path = dir.join("test/station.dat");
        let expected_station = get_station();
        let delim = '\n' as u8;

        let station = SimpleStationReader::from_file(path, delim).unwrap();

        assert_eq!(expected_station, station);
    }

    #[test]
    fn parse_invalid_datetime_str() {
        const INVALID_DATETIME_ITEM_STR: &str = "20150101-000000	41.186897	-125.424376	410.365252";

        let invalid_datetime_res = SimpleStationReader::parse_item_str(INVALID_DATETIME_ITEM_STR);
        assert!(matches!(invalid_datetime_res, Err(StationError::Parse(_))));
    }

    #[test]
    #[should_panic]
    fn parse_invalid_location_str() {
        const INVALID_LOCATION_ITEM_STR: &str = "20150101   000000	empty	-125.424376	410.365252";

        let _ = SimpleStationReader::parse_item_str(INVALID_LOCATION_ITEM_STR);
    }

    #[test]
    fn invalid_file() {
        const NONEXISTENT_PATH: &str = "";

        assert!(matches!(
            SimpleStationReader::from_file_by_lines(NONEXISTENT_PATH),
            Err(StationError::IO(_))
        ));
    }
}
