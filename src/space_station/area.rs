use crate::geom::point::Point;

pub trait AreaTrait {
    fn contains(&self, point: &Point) -> bool;
}
