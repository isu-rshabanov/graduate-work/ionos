use chrono::NaiveDateTime;

use crate::geom::point::Point;

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct StationRecord {
    datetime: NaiveDateTime,
    location: Point,
}

impl StationRecord {
    pub fn new(datetime: NaiveDateTime, location: Point) -> Self {
        Self { datetime, location }
    }

    pub fn get_location(&self) -> &Point {
        &self.location
    }

    pub fn get_datetime(&self) -> &NaiveDateTime {
        &self.datetime
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn new_test() {
        let datetime = NaiveDateTime::default();
        let location = Point::new(0.0, 0.0, 0.0);

        let record = StationRecord::new(datetime, location.clone());

        assert_eq!(datetime, record.datetime);
        assert_eq!(location, record.location);
    }

    #[test]
    fn get_location_test() {
        let expected = Point::new(0.0, 0.0, 0.0);
        let record = StationRecord {
            datetime: NaiveDateTime::default(),
            location: Point::new(0.0, 0.0, 0.0),
        };

        assert!(expected.eq(record.get_location()));
    }

    #[test]
    fn get_datetime_test() {
        let expected = NaiveDateTime::default();
        let record = StationRecord {
            datetime: expected,
            location: Point::new(0.0, 0.0, 0.0),
        };

        assert!(expected.eq(record.get_datetime()));
    }
}
