use chrono::NaiveDateTime;
use thiserror::Error;

use super::{area::AreaTrait, station_record::StationRecord, ParseErrorKind};

const MILLIS_IN_SEC: u64 = 1000;

#[derive(Debug, PartialEq, Eq)]
pub struct Station {
    records: Vec<StationRecord>,
}

#[derive(Debug, Error)]
pub enum StationError {
    #[error("failed to parse: {0:?}")]
    Parse(#[from] ParseErrorKind),
    #[error("{0:?}")]
    IO(#[from] std::io::Error),
}

impl Station {
    pub fn records(&self) -> Records {
        Records {
            iter: self.records.iter(),
        }
    }

    pub fn traces_in_area(&self, area: &impl AreaTrait) -> Traces {
        let mut traces = Vec::default();

        // TODO: change from fold method to group_by when the latter is stabilized
        let left_records = self
            .records
            .iter()
            .filter(|x| area.contains(x.get_location()))
            .fold(Vec::<&StationRecord>::default(), |mut records, x| {
                if let Some(last) = records.last() {
                    if !self.is_time_seq(last.get_datetime(), x.get_datetime()) {
                        traces.push(Trace {
                            records: records.into_iter(),
                        });

                        records = Vec::default();
                    }
                }

                records.push(x);
                records
            });

        if !left_records.is_empty() {
            traces.push(Trace {
                records: left_records.into_iter(),
            });
        }

        Traces {
            traces: traces.into_iter(),
        }
    }

    #[inline(always)]
    fn is_time_seq(&self, lhs: &NaiveDateTime, rhs: &NaiveDateTime) -> bool {
        lhs.timestamp_millis().abs_diff(rhs.timestamp_millis()) <= MILLIS_IN_SEC
    }
}

#[derive(Debug)]
pub struct Records<'a> {
    iter: std::slice::Iter<'a, StationRecord>,
}

impl<'a> Iterator for Records<'a> {
    type Item = &'a StationRecord;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}

#[derive(Debug)]
pub struct Traces<'a> {
    traces: std::vec::IntoIter<Trace<'a>>,
}

#[derive(Debug, Clone)]
pub struct Trace<'a> {
    records: std::vec::IntoIter<&'a StationRecord>,
}

impl<'a> Iterator for Traces<'a> {
    type Item = Trace<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        self.traces.next()
    }
}

impl<'a> Iterator for Trace<'a> {
    type Item = &'a StationRecord;

    fn next(&mut self) -> Option<Self::Item> {
        self.records.next()
    }
}

impl FromIterator<StationRecord> for Station {
    fn from_iter<T: IntoIterator<Item = StationRecord>>(iter: T) -> Self {
        Self {
            records: iter.into_iter().collect(),
        }
    }
}

impl<'a> FromIterator<&'a StationRecord> for Station {
    fn from_iter<T: IntoIterator<Item = &'a StationRecord>>(iter: T) -> Self {
        Self {
            records: iter.into_iter().cloned().collect(),
        }
    }
}

impl<'a> FromIterator<Trace<'a>> for Station {
    fn from_iter<T: IntoIterator<Item = Trace<'a>>>(iter: T) -> Self {
        Self {
            records: iter.into_iter().flat_map(|x| x.cloned()).collect(),
        }
    }
}

#[cfg(test)]
mod tests {
    use chrono::Timelike;

    use crate::geom::point::Point;

    use super::*;

    struct TestArea;

    impl AreaTrait for TestArea {
        fn contains(&self, _: &Point) -> bool {
            true
        }
    }

    fn get_station() -> Station {
        Station {
            records: Vec::from([
                StationRecord::new(NaiveDateTime::default(), Point::default()),
                StationRecord::new(
                    NaiveDateTime::default().with_second(1).unwrap(),
                    Point::new(1.0, 0.0, 0.0),
                ),
                StationRecord::new(
                    NaiveDateTime::default().with_second(30).unwrap(),
                    Point::new(1.0, 1.0, 0.0),
                ),
                StationRecord::new(
                    NaiveDateTime::default().with_hour(1).unwrap(),
                    Point::new(1.0, 1.0, 1.0),
                ),
            ]),
        }
    }

    #[test]
    fn records() {
        let station = get_station();

        assert_eq!(
            station.records,
            station.records().cloned().collect::<Vec<_>>()
        );
    }

    #[test]
    fn is_time_seq() {
        let station = get_station();
        let lhs = NaiveDateTime::default();
        let rhs = NaiveDateTime::default().with_second(1).unwrap();

        assert!(station.is_time_seq(&lhs, &lhs));
        assert!(station.is_time_seq(&lhs, &rhs));

        let rhs = NaiveDateTime::default().with_second(2).unwrap();
        assert!(!station.is_time_seq(&lhs, &rhs));
    }

    #[test]
    fn traces_in_area() {
        let station = get_station();
        let area = TestArea;
        let total_size = station.records.len();

        let traces = station.traces_in_area(&area);
        let mut actual_total_size = 0;

        for mut trace in traces {
            let mut first = trace.next().expect("item was expected");
            actual_total_size += 1;

            for item in trace {
                assert!(station.is_time_seq(item.get_datetime(), first.get_datetime()));

                first = item;
                actual_total_size += 1;
            }
        }

        assert_eq!(total_size, actual_total_size);
    }

    #[test]
    fn station_from_records() {
        let expected_station = get_station();

        let actual_station = expected_station.records().collect::<Station>();

        assert_eq!(expected_station, actual_station);
    }

    #[test]
    fn station_from_traces() {
        let expected_station = get_station();
        let area = TestArea;
        let traces = expected_station.traces_in_area(&area);

        let actual_station = traces.collect::<Station>();

        assert_eq!(expected_station, actual_station);
    }

    #[test]
    fn station_from_vec_records() {
        let expected_station = get_station();
        let records = expected_station.records().cloned().collect::<Vec<_>>();

        let actual_station = records.into_iter().collect::<Station>();

        assert_eq!(expected_station, actual_station);
    }
}
