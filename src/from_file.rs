use std::path::Path;

pub trait FromFileByLines {
    type Err;

    type Item;

    type Target;

    /// Parse a string record representation of Self::Item
    fn parse_item_str(record: &str) -> Result<Self::Item, Self::Err>;

    /// Creates a Self::Target object from a text file
    fn from_file_by_lines<P: AsRef<Path>>(path: P) -> Result<Self::Target, Self::Err>;
}

pub trait FromFile {
    type Err;

    type Item;

    type Target;

    /// Parse a byte array record representation of Self::Item
    fn parse_item(record: &[u8]) -> Result<Self::Item, Self::Err>;

    /// Creates a Self::Target object from a file
    fn from_file<P: AsRef<Path>>(path: P, delim: u8) -> Result<Self::Target, Self::Err>;
}
